module.exports = function keyMirror(obj) {
  return Object.keys(obj).reduce(function(acc, next) {
    return acc[next] = next, acc;
  }, {});
};