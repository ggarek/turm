var Promise = require('bluebird');
var HttpMethods = require('./HttpMethods');

function assert(value, message) {
  if(!value) throw new Error('Assert violation' + (message ? 'with message: "' + message + '.' : '.'));
}

function DataClient() {}

function mergeHeaders (primary, secondary) {
  if(!primary || primary.length <= 0) return secondary;
  if(!secondary || secondary.length <= 0) return primary;

  primary = primary.concat();
  secondary = secondary.concat();

  var merged = [];
  var i = 0;
  var j = 0;

  // Copy all primary headers
  for(i = 0; i < primary.length; i++){
    merged.push(primary[i]);

    // Skip all secondary headers with same name
    while(secondary[j][0] === primary[i][0]){
      j++;
    }
  }

  // Copy all secondary headers left
  for(j; j<secondary.length; j++){
    merged.push(secondary[j]);
  }

  return merged;
}

DataClient.prototype = {
  createRequest: function (data) {
    var request = {
      method: 'GET',
      url: data.url,
      payload: data.payload,
      headers: data.headers
    };

    if(HttpMethods[data.method] === data.method){
      request.method = data.method;
    }

    return request;
  },

  /**
   * Could be overriden to provide special headers for each request
   * @returns {Array} [['HEADER', 'VALUE'], ..]
   */
  getHeaders: function () {
    return [];
  },

  send: function (request) {
    assert(request);
    assert(request.method);
    assert(request.url);

    return new Promise(function (resolve, reject) {
      var xhr = new XMLHttpRequest();

      xhr.onload = function () {
        if(xhr.status === 200){
          resolve(xhr.response);
        } else {
          reject(xhr.status, xhr.response);
        }
      };

      // Setup xhr props
      xhr.open(request.method || 'GET', request.url);

      mergeHeaders(this.getHeaders(), request.headers).forEach(function (header) {
        xhr.setRequestHeader(header[0], header[1]);
      });

      if(request.payload) {
        xhr.send(request.payload);
      } else {
        xhr.send();
      }
    }.bind(this));
  },

  delete: function (url) {
    return this.send(this.createRequest({
      method: HttpMethods.DELETE,
      url: url
    }))
  },

  sendJSON: function (method, url, payload){
    assert(payload === undefined || typeof payload === 'object', 'JSON payload if provided should be of type "Object"');

    var data = {
      method: method,
      url: url,
      headers: [
        ['Content-Type', 'application/json;charset=UTF-8']
      ]
    };

    if(payload !== undefined) {
      data.payload = JSON.stringify(payload);
    }

    return this.send(this.createRequest(data)).then(function (response) {
      return JSON.parse(response);
    })
  },

  getJSON: function (url) {
    return this.sendJSON(HttpMethods.GET, url);
  },

  postJSON: function (url, payload) {
    return this.sendJSON(HttpMethods.POST, url, payload);
  },

  putJSON: function (url, payload) {
    return this.sendJSON(HttpMethods.PUT, url, payload);
  }
};

module.exports = DataClient;