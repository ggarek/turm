var keyMirror = require('./utils/keyMirror');

module.exports = keyMirror({
  DELETE: null,
  GET: null,
  HEAD: null,
  PATH: null,
  POST: null,
  PUT: null
});
